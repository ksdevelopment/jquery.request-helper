/**
 * Methods:
 *
 * defaultAjaxRequest
 * defaultAjaxForm
 *
 *
 * @licence proportary
 * @author Kasper Brøgger Svendsen
 * @copyright All rights reserved © 2016 KS Development ApS
 */
(function ($) {
    $.defaultAjaxRequestHelper = {
        defaults: {
            rules: {},
            messages: {},
            ignore: ":hidden",
            action: null,
            method: null,
            emulate_method: true,
            headers: {},
            dataType: 'json',

            blockUI: true,

            before_submit_callback: null,

            success_redirect_url: null,
            success_reload: false,
            success_callback: null,

            alert_error: true,
            error_callback: null,
            error_redirect_url: null,
            error_reload: null,

            success_validator: function (data, textStatus, jqXHR, options) {
                if (options.dataType == 'json') {
                    return (typeof data === 'object' && data.status && /^ok$/i.test(data.status));
                } else {
                    return jqXHR.status >= 200 && jqXHR.status <= 206;
                }
            },

            error_placement: function (error, element) {
                var $form_row = element.closest('.form-row');
                if (element.is(':radio')) {
                    error.appendTo(element.parents('.checks'));
                } else if ($form_row.length !== 0) {
                    error.appendTo($form_row);
                } else {
                    error.insertAfter(element)
                }
                error.css('width', 'auto');
            }
        }
    };


    // bind to $.defaultAjaxRequest
    $.defaultAjaxRequest = function (action, method, data, options) {
        options = $.extend({}, $.defaultAjaxRequestHelper.defaults, options);
        var self = this;

        if (options.blockUI && $(window).data('blockUI.isBlocked') !== 1)
            $.blockUI();

        // convert array to FormData object
        if (typeof data.append !== 'function' && !(data instanceof FormData) && typeof data === 'object') {
            var formdata = new FormData();
            appendFormdata(formdata, data);
            data = formdata;
        }

        if (typeof $.defaultAjaxRequest.defaults.data === 'object') {
            Object.keys($.defaultAjaxRequest.defaults.data).forEach(function (key) {
                if (!data.has(key)) {
                    data.append(key, $.defaultAjaxRequest.defaults.data[key]);
                }
            });
        }

        return $.ajax({
            url: action,
            type: method,
            dataType: options.dataType,
            data: data,
            mimeTypes: "multipart/form-data",
            contentType: false,
            cache: false,
            headers: options.headers,
            processData: false
        })
                .done(function (data, textStatus, jqXHR) {
                    if (options.success_validator(data, textStatus, jqXHR, options)) {
                        successHandler(options, data, jqXHR);
                    } else {
                        errorHandler(options, jqXHR);
                    }

                })
                .fail(function (jqXHR, textStatus, errorThrown) {
                    errorHandler(options, jqXHR);
                });
    };


    $.defaultAjaxRequest.defaults = {
        data: {}
    };

    // bind to elements (form)
    $.fn.defaultAjaxForm = function (options) {
        if ($(this).length === 0)
            return;

        options = $.extend({}, $.defaultAjaxRequestHelper.defaults, options);
        var self = this;

        self.validate({
            rules: options.rules,
            ignore: options.ignore,
            messages: options.messages,
            errorPlacement: options.error_placement,
            submitHandler: function (form) {
                var formData = new FormData(form);
                var blob_promises = self.find('img.cropper-hidden').map(function () {
                    return cropperToBlob($(this));
                });

                $.when.apply($, blob_promises).then(function () {

                    $.each(arguments, function (i, obj) {
                        formData.append(obj.name, obj.blob, obj.filename);
                    });

                    if (typeof (options.before_submit_callback) === 'function') {
                        formData = options.before_submit_callback(formData);
                    }

                    var method = (options.method || self.find('input[name=_method]').val() || self.attr('method') || 'post');
                    if (options.emulate_method) {
                        formData.append('_method', method);
                        if (!/^get$/i.test(method))
                            method = 'POST';
                    }

                    var action = self.attr('action');

                    $.defaultAjaxRequest(action, method, formData, options);
                });


            }
        });

        return this;
    };


    function appendFormdata(FormData, data, name) {
        name = name || '';
        if (data !== null && typeof data === 'object') {
            $.each(data, function (index, value) {
                if (name == '') {
                    appendFormdata(FormData, value, index);
                } else {
                    appendFormdata(FormData, value, name + '[' + index + ']');
                }
            })
        } else {
            FormData.append(name, data);
        }
    }

    var cropperToBlob = function ($element) {
        var d = $.Deferred();

        if ($element.data('cropper')) {
            var $cropper = $element.data('cropper');
            $cropper.getCroppedCanvas().toBlob(function (blob) {
                    d.resolve({blob: blob, name: $element.data('name'), filename: $element.data('filename')});
                }, $element.data('type')
            );
        } else {
            $element.cropper('getCroppedCanvas').toBlob(function (blob) {
                    d.resolve({blob: blob, name: $element.data('name'), filename: $element.data('filename')});
                }, $element.data('type')
            );
        }

        return d.promise();
    };

    var errorHandler = function (options, jqXHR) {
        if (options.alert_error) {

            var json = null;
            try {
                json = JSON.parse(jqXHR.responseText);
            } catch (e) {
            }

            if (jqXHR.status === 422 && json && /^validation_error$/i.test(json.status)) {
                var validation_errors = $.map(json.messages, function (errors, field) {
                    return errors;
                });

                alert("Valideringsfejl:\n" + validation_errors.join("\n"));
            } else if (jqXHR.status === 422 && json && typeof json.error !== 'undefined' && typeof json.error.errors === "object") {
                var validation_errors = $.map(json.error.errors, function (errors, field) {
                    return errors;
                });

                alert(validation_errors.join("\n"));
            } else if (jqXHR.status === 422 && json && typeof json.errors !== 'undefined' && typeof json.errors === "object") {
                var validation_errors = [];
                $.each(json.errors, function (field, errors) {
                    $.each(errors, function (i, error) {
                        if (typeof error === 'string') {
                            validation_errors.push(error)
                        }
                    })
                })
                alert(validation_errors.join("\n"));
            } else if (json && json.error && typeof json.error.message !== "undefined") {
                alert(json.error.message);
            } else if (json && json.message) {
                alert(json.message);
            } else if (json && json.msg) {
                alert(json.msg);
            } else {
                alert('Ukendt fejl (' + jqXHR.status + ')');
            }

        }

        if (typeof (options.error_callback) === 'function') {
            var result = options.error_callback.call(self, jqXHR.responseText, jqXHR);

            if (result === false) {
                if (options.blockUI)
                    $.unblockUI();

                return;
            }
        }

        if (options.error_redirect_url) {
            location.href = options.error_redirect_url;
            return;
        }

        if (options.error_reload === true) {
            location.reload();
            return;
        }

        // if here, no error handler - then unblock UI if exists
        if (options.blockUI)
            $.unblockUI();
    };

    var successHandler = function (options, data, jqXHR) {

        if (typeof (options.success_callback) === 'function') {
            var result = options.success_callback.call(self, data, jqXHR);

            if (result === false) {
                if (options.blockUI)
                    $.unblockUI();

                return;
            }
        }

        if (options.success_redirect_url) {
            location.href = (typeof options.success_redirect_url === 'function') ? options.success_redirect_url(data) : options.success_redirect_url;
            return;
        }

        if (options.success_reload) {
            location.reload();
            return;
        }

        // if here, no success handler - then unblock UI if exists
        if (options.blockUI)
            $.unblockUI();
    };
}(jQuery));
